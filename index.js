require('./env');
const app = require('./app/App.js');

app.initialize();

module.exports = app;
