require('../utils/lib/handleModule');
const { gql } = require('apollo-server');
const News = require('./news/datasource/News.js');
const newsResolver = require('./news/resolvers/index.js');
const newsSchema = require('./news/queries/schema.graphql');

const typeDefs = gql`
    type Query {
        _dummy: String
    }
    ${newsSchema}
`;

const Resolvers = [newsResolver];

module.exports = { typeDefs, News, Resolvers };
