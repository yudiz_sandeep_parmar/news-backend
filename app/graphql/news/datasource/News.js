const { RESTDataSource } = require('apollo-datasource-rest');
class News extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://newsapi.org/v2/';
    this.API_KEY = process.env.NEWS_API_KEY;
  }

  async countryNews(params) {
    const reqOptions = { mode: 'cors', headers: { 'Access-Control-Allow-Origin': '*' } };
    if (this.API_KEY) {
      reqOptions.headers['X-Api-Key'] = this.API_KEY;
    }
    return this.get(`top-headlines?country=${params.country || 'IN'}&apiKey=${this.API_KEY}`);
  }
}

module.exports = News;
