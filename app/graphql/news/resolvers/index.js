const resolvers = {
  Query: {
    getCountryNews: async(_, input, context) => {
      return context.dataSources.news.countryNews(input);
    }
  }
};

module.exports = resolvers;
