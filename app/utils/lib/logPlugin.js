const log = require('./log.js');

const logPlugin = {
  async requestDidStart(initialRequestContext) {
    log.blue('Request started! Query:\n' + initialRequestContext.request.query);
    return {
      async executionDidStart(executionRequestContext) {
        log.blue(`Execution Context: ${executionRequestContext}`);
        return {
          willResolveField({ source, args, context, info }) {
            const start = process.hrtime.bigint();
            return (error, result) => {
              const end = process.hrtime.bigint();
              log.blue(`Field ${info.parentType.name}.${info.fieldName} took ${end - start}ns`);
              if (error) {
                log.red(`It failed with ${error}`);
              } else {
                log.green(`It returned ${result}`);
              }
            };
          }
        };
      }
    };
  }

};

module.exports = log;
