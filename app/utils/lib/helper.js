/* eslint-disable no-prototype-builtins */
const jwt = require('jsonwebtoken');
const { messages, status, jsonStatus } = require('./api.response');
const log = require('./log.js');

const _ = {};

_.verifyToken = function(token) {
  try {
    return jwt.verify(token, process.env.TOKEN, function(err, decoded) {
      return err ? err.message : decoded; // return true if token expired
    });
  } catch (error) {
    return error ? error.message : error;
  }
};

_.catchError = (name, error, req, res) => {
  _.handleCatchError(error);
  return res.status(status.InternalServerError).jsonp({
    status: jsonStatus.InternalServerError,
    message: messages.error
  });
};

_.handleCatchError = (error) => {
  log.red('**********ERROR***********', error);
};

module.exports = _;
