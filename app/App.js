const { ApolloServer } = require('apollo-server');
const { log } = require('./utils/index.js');
const { typeDefs, News, Resolvers } = require('./graphql/index.js');
const { makeExecutableSchema } = require('@graphql-tools/schema');
const schema = makeExecutableSchema({
  typeDefs,
  resolvers: Resolvers
});
class App {
  constructor() {
    this.corsOptions = {
      origin: [process.env.BASE_URL, 'https://studio.apollographql.com'],
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type', 'Authorization']
    };
  }

  initialize() {
    this.setupServer();
  }

  async setupServer() {
    this.app = new ApolloServer({
      schema,
      dataSources: () => {
        return {
          news: new News()
        };
      },
      csrfPrevention: true,
      cache: 'bounded'
    });
    const { url } = await this.app.listen();
    log.blue(`🚀 🚀 🚀   ${url}graphql`);
  };
}

module.exports = new App();
